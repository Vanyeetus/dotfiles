# If not running interactively, don't do anything
    [[ $- != *i* ]] && return

# History configuration (unlimited history)
    export HISTFILESIZE=
    export HISTSIZE=
    export HISTTIMEFORMAT="%F %T | "
    export HISTFILE=~/.bash_history
    export PROMPT_COMMAND='history -a'

# Shell options (shopt)
    shopt -s cdspell
    shopt -s histappend
    shopt -s checkwinsize
    shopt -s cmdhist
    shopt -s lithist
    stty -ixon # CTRL-s history search

# Prompt shell style
    PS1='[\u@\h \W]\$ '

# Aliases
    alias bruh='sudo $(history -p !!)'
    alias clear='clear -x' # Don't clear scrollback history (-x)
    alias cd..='cd ..'
    alias ..='cd ..'
    alias ....='cd ../../'
    alias ......='cd ../../../'
    alias vi='nvim'

# Make grep have color
    alias grep='grep --color=auto'

# System exports
    export EDITOR="/usr/bin/nvim"

