#!/bin/sh

set -e

if [ ! -f /usr/bin/stow ]; then
    stow="$HOME/.dotfiles/bin/./stash"
    echo "Using bundled stash"
else
    stow="/usr/bin/stow"
    echo "Using locally installed stow"
fi


if [ -f ~/.bashrc ]; then
    printf ".bashrc already exists. Do you want to replace it?\n[y/N]: "; read -r bashrc_choice
    if [ "$bashrc_choice" = "y" ]; then
        mv ~/.bashrc ~/.bashrc.old
        "$stow" bash
    fi
fi


# Iterate over directories.
for dir in ~/.dotfiles/*/; do
    [ -d "$dir" ] || continue
    dir="$(basename "$dir")"

    case "$dir" in
        misc|bin|scripts|options|icons) printf 'Skipping %s\n' "$dir";;
        *) "$stow" "$dir";;
    esac
done

