-- require("utils")

-- Colors
o.termguicolors = true

-- Lualine setup
require("lualine").setup({
    options = {
        theme = "onedark",
        section_separators = "",
        component_separators = "",
    },
})
