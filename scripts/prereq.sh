#!/bin/sh

set -e

prog_array="npm bash gcc fzf nvim nano tmux icu"
install_array=""

IFS=' ' # Set IFS to space

for program_name in $prog_array; do

    if  [ "$program_name" = "nvim" ]; then
        program_name="neovim"
    fi

    if command -v "$program_name"  > /dev/null 2>&1; then
        printf '%s exists, skipping...\n' "$program_name"
    else
        install_array="${install_array} $program_name "
    fi
done

# Print the install_array
echo "Programs to install: $install_array"

# Install

pacman -Sy $install_array --noconfirm
